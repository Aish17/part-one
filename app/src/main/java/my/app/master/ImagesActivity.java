package my.app.master;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import java.util.ArrayList;
import java.util.List;


public class ImagesActivity extends AppCompatActivity {



    private static RecyclerView.Adapter kvadapter;
    private RecyclerView.LayoutManager kvlayoutManager;
    private static RecyclerView kvrecyclerView;
    ArrayList <String> uriStr = new ArrayList<String>();
    List<Upload> urlss;

    private FirebaseStorage kvStorage;
    private StorageReference kvStorageRef;
    private List<Upload> kvFileURL;
    private String OwnerId;



    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_images);
        final String [] fileURL;
        kvStorage = FirebaseStorage.getInstance();
        FirebaseUser userInfo = FirebaseAuth.getInstance().getCurrentUser();
        OwnerId=userInfo.getUid();
        kvStorageRef = kvStorage.getReference().child("images/"+OwnerId);

        Toast.makeText(this, "Testing the Image in the reference", Toast.LENGTH_SHORT).show();

        //final ImageView imageView = (ImageView) findViewById(R.id.myImg);
        //imageView.setImageDrawable(null);
        Log.v( "OUT","outside the loop");


        kvrecyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        kvrecyclerView.setHasFixedSize(true);

        kvlayoutManager = new GridLayoutManager(getApplicationContext(), 1);
        kvrecyclerView.setLayoutManager(kvlayoutManager);
        kvrecyclerView.setItemAnimator(new DefaultItemAnimator());
        FirebaseFirestore rootRef = FirebaseFirestore.getInstance();
        rootRef.collection("fileInfo").whereEqualTo("owner",OwnerId).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    List<Upload> list = new ArrayList<>();
                    for (DocumentSnapshot document : task.getResult()) {
                        if (document.exists()) {
                            Upload photo = document.toObject(Upload.class);
                            list.add(photo); //Add Photo object to the list
                        }

                        //Do what you need to do with your list
                    }
                    urlss=list;
                    setfirebaseAdapter();
                } else {
                    //Log.d("Test", "Error getting documents: ", task.getException());
                }
            }
        });


    }

    void setfirebaseAdapter(){

        //Log.e("URLSS SIZE "+ urlss.size(), "Size" );
        kvadapter = new ImagesAdapter(urlss);
        kvrecyclerView.setAdapter(kvadapter);
    }



}