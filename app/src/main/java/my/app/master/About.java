package my.app.master;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

public class About extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        //remove status bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    //Menu Inflate
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.nav_about,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.home:
                Intent intent = new Intent(About.this, MainActivity.class);
                startActivity(intent);
                return true;

            case R.id.contact:
                Intent intent1 = new Intent(About.this, Contact.class);
                startActivity(intent1);
                return true;

            case R.id.faq:
                Intent intent2 = new Intent(About.this, Faq.class);
                startActivity(intent2);
                return true;

            case R.id.uploads:
                Intent intent3 = new Intent(About.this, ImagesActivity.class);
                startActivity(intent3);
                return true;

            case R.id.signOut:
                Toast.makeText(About.this, "SignOut", Toast.LENGTH_LONG).show();
                signOut();
                Intent signUpAct = new Intent(About.this,SignUpActivity.class);
                startActivity(signUpAct);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void signOut() {
        // Firebase sign out
        FirebaseAuth.getInstance().signOut();


    }
}