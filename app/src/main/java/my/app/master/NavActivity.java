package my.app.master;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import android.app.Activity;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import android.view.Menu;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;

public class NavActivity extends AppCompatActivity {
    Toolbar toolbar;
    DrawerLayout drawerLayout;
    NavigationView navigationView;
    ActionBarDrawerToggle toggle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //remove status bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater mnuInflater = getMenuInflater();
        mnuInflater.inflate(R.menu.nav_menu, menu);
        Toast.makeText(this, "Menu Initiated", Toast.LENGTH_SHORT).show();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Toast.makeText(this, "Testing Menu " +item.getItemId(), Toast.LENGTH_SHORT).show();
        switch (item.getItemId()) {
            case R.id.home:
                return true;

            case R.id.uploads:
                return true;
            case R.id.contact:
                return true;
            case R.id.faq:
                return true;
            case R.id.signOut:
                Toast.makeText(NavActivity.this, "SignOut", Toast.LENGTH_SHORT).show();
                SignUpActivity sgnAct = new SignUpActivity();
                sgnAct.signOut();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



}